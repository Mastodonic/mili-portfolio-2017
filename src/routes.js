import Home from './components/Home.vue';
import Contact from './components/Contact.vue';
import About from './components/About.vue';
import Gallery from './components/Gallery.vue';

export default[
    {
        path : '/',
        name : 'web development',
        component : Home
    }, {
        path : '/photography/:category',
        name : 'photography',
        component : Gallery
    }, {
        path : '/photography/:category/:photoid',
        name : 'photo',
        component : Gallery
    }, {
        path : '/about',
        name : 'about',
        component : About
    }, {
        path : '/contact',
        name : 'contact',
        component : Contact
    }
];
