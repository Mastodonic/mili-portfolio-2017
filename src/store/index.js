import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

//Modules
import gallery from './modules/gallery';
import lightbox from './modules/lightbox';

export default new Vuex.Store({
    modules: {
        gallery,
        lightbox
    }
});
