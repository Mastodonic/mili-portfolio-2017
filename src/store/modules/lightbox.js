import router from 'vue-router';

const state = {
    currentImage: null,
    currentScrollPosition: 0,
    lightBoxIsOpen: false
};

const mutations = {
    setCurrentImage(state, payload) {
        state.currentImage = payload;
    },
    setCurrentScrollPosition(state, payload) {
        state.currentScrollPosition = payload;
    },
    toggleLightBox(state) {
        state.lightBoxIsOpen = !state.lightBoxIsOpen;
    },
    closeLightbox(state) {
        state.lightBoxIsOpen = false;
    },
    openLightbox(state) {
        state.lightBoxIsOpen = true;
    }
};

const getters = {
    prevImage(state, getters, rootState) {
        let imagesList = getters.imagesListComplete;
        let index = imagesList.indexOf(state.currentImage);
        return index > 0 ? imagesList[index - 1] : null;
    },
    nextImage(state, getters, rootState) {
        let imagesList = getters.imagesListComplete;
        let index = imagesList.indexOf(state.currentImage);
        return index !== -1 && index < imagesList.length ? imagesList[index + 1] : null;
    }
};

const actions = {
    loadImage(context, payload) {
        if (payload) {
            let thumbnail = new Image();
            let img = new Image();
            thumbnail.src = payload.thumbnailSrc;
            img.src = payload.mainSrc;
        }
    },
    setCurrentImageById(context, payload) {
        let img = context.getters.imagesListComplete.find((img) => {
            return img.id == payload;
        });
        context.commit('setCurrentImage', img);
    },
    disableScroll(context) {
        let body = document.body;
        let scrollTop = body.scrollTop;
        context.commit('setCurrentScrollPosition', scrollTop);
        body.style.top = -scrollTop + 'px';
        body.classList.add('body--no-scroll');
    },
    enableScroll(context) {
        let body = document.body;
        body.style.top = 0;
        body.classList.remove('body--no-scroll');
        body.scrollTop = context.state.currentScrollPosition;
    }
};

export default {
    state,
    getters,
    mutations,
    actions
};
