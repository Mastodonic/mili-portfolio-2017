import Vue from 'vue';
import Vuex from 'vuex';
import Vuefire from 'vuefire';
import Firebase from 'firebase';
import VueRouter from 'vue-router';
import modernizr from './helpers/modernizr';

Vue.use(Vuefire);
Vue.use(VueRouter);

//Firebase Database
let fbConfig = {
    apiKey: 'AIzaSyDi-WL9CZNgKgLxSReHjPGrAwYMrf0Ppt4',
    authDomain: 'mili-porfolio.firebaseapp.com',
    databaseURL: 'https://mili-porfolio.firebaseio.com',
    storageBucket: 'mili-porfolio.appspot.com',
    messagingSenderId: '110580568443'
};

let fbApp = Firebase.initializeApp(fbConfig);
let db = fbApp.database();
let imagesRef = db.ref('images');

//Store
import store from './store';

//Components
import App from './components/App.vue';

//Router
import routes from './routes';
const router = new VueRouter({
    routes,
    mode: 'history'
});

new Vue({
    el: '#app',
    template: '<App/>',
    store,
    router,
    firebase: {
        images: imagesRef
    },
    components: { App }
});
